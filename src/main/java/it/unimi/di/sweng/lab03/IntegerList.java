package it.unimi.di.sweng.lab03;

public class IntegerList {
	private Element head;
	private Element tail;
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("[");
		Element cur= head;
		while(cur != null){
			if(cur != head)
				result.append(" ");
			
			result.append(cur.value);
			cur=cur.next;
		}
		result.append("]");
		return result.toString();
	}

	public void addLast(int num) {
		Element elem=new Element();
		elem.value=num;
		elem.next=null;
		if(tail!=null)
			tail.next=elem;
		tail=elem;
		if(head==null)
			head=elem;
	}
	
	
	
	
	
	
	private class Element{
		private int value;
		private Element next;
	}
	
}
